﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BusinessLogic.Factories.Interfaces
{
    public interface ITokenFactory
    {
        Task<string> GenerateToken(IdentityUser user);
    }
}
