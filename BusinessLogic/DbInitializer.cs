﻿using BusinessLogic.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public static class DbInitializer
    {
        public static async Task InitializeAsync(IServiceProvider service)
        {
            using (var serviceScope = service.CreateScope())
            {
                var scopeServiceProvider = serviceScope.ServiceProvider;
                var userManager = scopeServiceProvider.GetService<UserManager<IdentityUser>>();
                var db = scopeServiceProvider.GetService<MySqlContext>();
                await InitializeData(db, userManager);
            }
        }

        public static async Task InitializeData(MySqlContext context, UserManager<IdentityUser> userManager)
        {
            context.Database.EnsureCreated();

            if (!context.Tree.Any())
            {

                List<TreeNode> childrenLevel1 = new List<TreeNode>()
                {
                    new TreeNode{ Name="Dokumenty", Children = new List<TreeNode>()
                        {
                            new TreeNode{ Name="Moje" },
                            new TreeNode{ Name="Inne", Children = new List<TreeNode>()
                                {
                                    new TreeNode{ Name="Temp" },
                                } },
                        }},
                    new TreeNode{ Name="Muzyka" },
                    new TreeNode{ Name="Obrazy" },
                    new TreeNode{ Name="Filmy" , Children = new List<TreeNode>()
                        {
                            new TreeNode{ Name="Moje" },
                            new TreeNode{ Name="Inne", Children = new List<TreeNode>()
                                {
                                    new TreeNode{ Name="Temp" },
                                } },
                        }},
                    new TreeNode{ Name="Pobrane" },
                    new TreeNode{ Name="Ulubione" }
                };

                TreeNode root = new TreeNode()
                {
                    Name = "Komputer",
                    Children = childrenLevel1
                };

                context.Tree.Add(root);
            }

            if (!context.Users.Any())
            {
                var user = new IdentityUser { UserName = "admin"};
                var result = await userManager.CreateAsync(user, "Admin123?");
            }

            await context.SaveChangesAsync();
        }
    }
}
