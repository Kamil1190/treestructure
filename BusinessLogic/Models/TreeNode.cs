﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.Models
{
    public class TreeNode
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public String Name { get; set; }
        public TreeNode Parent { get; set; }
        public ICollection<TreeNode> Children { get; set; }
    }
}
