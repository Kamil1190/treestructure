﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.DTO
{
    public class TreeNodeResponseDto : TreeNodeRequestDto
    {
        public Guid Id { get; set; }
    }
}
