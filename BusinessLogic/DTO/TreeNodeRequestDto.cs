﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessLogic.DTO
{
    public class TreeNodeRequestDto
    {
        [Required]
        public String Name { get; set; }
        public ICollection<TreeNodeResponseDto> Children { get; set; }
    }
}
