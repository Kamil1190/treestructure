﻿using AutoMapper;
using BusinessLogic.DTO;
using BusinessLogic.Factories.Interfaces;
using BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class AccountService : IAccountService
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly ITokenFactory tokenFactory;
        private readonly IMapper mapper;

        public AccountService(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, ITokenFactory tokenFactory, IMapper mapper)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.tokenFactory = tokenFactory;
            this.mapper = mapper;
        }

        public async Task<object> Login(LoginDto model)
        {
            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                throw new ValidationException("User not found");
            }

            var result = await signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);

            if (result.Succeeded)
            {
                return await tokenFactory.GenerateToken(user);
            }

            throw new ValidationException("Invalid login attempt.");
        }
    }
}
