﻿using AutoMapper;
using BusinessLogic.DTO;
using BusinessLogic.Models;
using BusinessLogic.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public class TreeService : ITreeService
    {
        private readonly MySqlContext context;
        private readonly IMapper mapper;

        public TreeService(MySqlContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<bool> Add(TreeNodeRequestDto nodeDto, Guid parentId)
        {
            var parent = await context.Tree.FindAsync(parentId);
            if(parent == null)
            {
                throw new ValidationException("Parent not found.");
            }

            if(parent.Children == null)
            {
                parent.Children = new List<TreeNode>();
            }

            var node = mapper.Map<TreeNode>(nodeDto);
            parent.Children.Add(node);

            return await context.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<bool> Delete(Guid id)
        {
            var node = await context.Tree.FindAsync(id);
            if (node == null)
            {
                throw new ValidationException("Node not found.");
            }

            await _RemoveChildren(node);

            context.Tree.Remove(node);

            return await context.SaveChangesAsync() > 0 ? true : false;
        }

        public async Task<TreeNodeResponseDto> GetRoot()
        {
            var root = await context.Tree.SingleAsync(t => t.Parent == null);
            _LoadChildren(root);
            return mapper.Map<TreeNodeResponseDto>(root);
        }

        public async Task<bool> UpdateName(TreeNodeResponseDto nodeDto)
        {
            var node = await context.Tree.FindAsync(nodeDto.Id);
            if(node == null)
            {
                throw new ValidationException("Node not found.");
            }

            node.Name = nodeDto.Name;

            return await context.SaveChangesAsync() > 0 ? true : false;
        }

        private void _LoadChildren(TreeNode node)
        {
            context.Entry(node).Collection(n => n.Children).Load();

            foreach (var nodeChild in node.Children)
            {
                if (node.Children != null)
                {
                    _LoadChildren(nodeChild);
                }
            }
        }

        private async Task _RemoveChildren(TreeNode node)
        {
            context.Entry(node).Collection(n => n.Children).Load();

            foreach (var nodeChild in node.Children)
            {
                if (node.Children != null)
                {
                    await _RemoveChildren(nodeChild);
                }
            }

            context.Tree.RemoveRange(node.Children);
            await context.SaveChangesAsync();
        }

    }
}
