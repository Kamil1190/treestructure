﻿using BusinessLogic.DTO;
using System;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Interfaces
{
    public interface ITreeService
    {
        Task<TreeNodeResponseDto> GetRoot();
        Task<bool> UpdateName(TreeNodeResponseDto nodeDto);
        Task<bool> Add(TreeNodeRequestDto nodeDto, Guid parentId);
        Task<bool> Delete(Guid id);
    }
}
