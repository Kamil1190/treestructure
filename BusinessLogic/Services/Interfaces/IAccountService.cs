﻿using BusinessLogic.DTO;
using System.Threading.Tasks;

namespace BusinessLogic.Services.Interfaces
{
    public interface IAccountService
    {
        Task<object> Login(LoginDto model);
    }
}
