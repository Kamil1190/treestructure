﻿using AutoMapper;
using BusinessLogic.DTO;
using BusinessLogic.Models;

namespace BusinessLogic.MappingProfiles
{
    public class TreeMappingProfile : Profile
    {
        public TreeMappingProfile()
        {
            CreateMap<TreeNodeResponseDto, TreeNode>();
            CreateMap<TreeNodeRequestDto, TreeNode>();
            CreateMap<TreeNode, TreeNodeRequestDto>();
            CreateMap<TreeNode, TreeNodeResponseDto>();
        }
    }
}
