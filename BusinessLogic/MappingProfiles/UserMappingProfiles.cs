﻿using AutoMapper;
using BusinessLogic.DTO;
using Microsoft.AspNetCore.Identity;

namespace BusinessLogic.MappingProfiles
{
    public class UserMappingProfiles : Profile
    {
        public UserMappingProfiles()
        {
            CreateMap<LoginDto, IdentityUser>();
        }
    }
}
