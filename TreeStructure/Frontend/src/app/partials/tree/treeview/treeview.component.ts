import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TreeService } from '../tree.service';
import { Treenode } from '../../../models/treenode';
import { AuthorizationService } from '../../authorization/authorization.service';

@Component({
  selector: 'app-treeview',
  templateUrl: './treeview.component.html',
  styleUrls: ['./treeview.component.css']
})
export class TreeviewComponent implements OnInit {

  tree: Treenode;
  message: string;
  createNoodeForm: FormGroup;
  updateNoodeForm: FormGroup;
  selectedNode: Treenode;

  constructor(private treeService: TreeService, private authorizationService: AuthorizationService, private formBuilder: FormBuilder) {
    this.loadTree();
    this.createNoodeForm = formBuilder.group({
      'Name': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
    });
    this.updateNoodeForm = formBuilder.group({
      'Name': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
      'Id': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
    });
  }

  public options = {
    allowDrag: true,
    allowDrop: true
  };

  loadTree(){
    this.treeService.getTreeRoot()
      .subscribe(
        treeRoot => this.tree = treeRoot,
        error => this.message = error);
  }

  onDropDelete($event) {
    this.treeService.delete($event.element.data.id)
      .subscribe(res => this.loadTree(), error => this.message = error.error);

    this.selectedNode = new Treenode();
  }

  createNoode(node: Treenode, id: string){
    this.treeService.create(node, id)
      .subscribe(data => this.loadTree(),error => this.message = error.error);
  }

  updateNode(node: Treenode){
    this.treeService.update(node)
      .subscribe(data => this.loadTree(),error => this.message = error.error);
  }

  updateSelected($event) {
    this.selectedNode = $event.node.data;
  }

  ngOnInit(): void {

  }

}
