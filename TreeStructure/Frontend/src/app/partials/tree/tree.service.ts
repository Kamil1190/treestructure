import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { baseApiUrl } from '../../resources/apiconfiguration';
import { Treenode } from '../../models/treenode';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


export const treeUrl = baseApiUrl + 'api/Tree/';

@Injectable()
export class TreeService {

  constructor(private http: HttpClient) { }

  getTreeRoot(): Observable<Treenode> {
      return this.http.get(treeUrl + 'GetRoot/')
            .map((res) => <Treenode>res)
            .catch(this.errorhandler);
  }

  create(node: Treenode, idParent: string): Observable<boolean> {
      return this.http.post(treeUrl + 'Create/' + idParent, node)
            .map((res) => <boolean>res)
            .catch(this.errorhandler);
  }

  update(node: Treenode): Observable<boolean> {
      return this.http.put(treeUrl + 'Update/', node)
            .map((res) => <boolean>res)
            .catch(this.errorhandler);
  }

  delete(id: string): Observable<boolean> {
      return this.http.delete(treeUrl + 'Delete/'+id)
            .map((res) => <boolean>res)
            .catch(this.errorhandler);
  }

  private errorhandler(error: Response) {
      return Observable.throw(error.json() || 'Server error');
  }
}
