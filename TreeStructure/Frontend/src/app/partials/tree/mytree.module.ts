import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TreeModule } from 'angular-tree-component';
import { TreeviewComponent } from './treeview/treeview.component';
import { TreeService } from './tree.service';

@NgModule({
  imports: [
    CommonModule,
    TreeModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [TreeService],
  declarations: [TreeviewComponent],
  exports: [TreeviewComponent]
})
export class MyTreeModule { }
