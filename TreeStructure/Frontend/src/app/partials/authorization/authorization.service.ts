import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import { User, IUser } from '../../models/user';
import { Userlogin } from '../../models/userlogin';
import { UserLocalStorage } from '../../models/user-local-storage';
import { baseApiUrl } from '../../resources/apiconfiguration';
import { JwtHelperService } from '@auth0/angular-jwt';


export const accountUrl = 'api/Account/';

@Injectable()
export class AuthorizationService {

  public currentUser: User = new User();
  public currentUserChange: Subject<IUser> = new Subject<IUser>();
  public helper: JwtHelperService = new JwtHelperService();

  constructor(private http: HttpClient, private router: Router) { 
    let user = JSON.parse(localStorage.getItem('currentUser')) as UserLocalStorage;
    if(user){
      this.setUser(user);
    }
  }

  login(userlogin: Userlogin): Observable<boolean> {
    if(localStorage.getItem('currentUser')){
      this.logout();
    }

    return this.http.post<any>(baseApiUrl+accountUrl+'Login', userlogin)
      .map(token => {
        if(token){
          var decodedToken = this.helper.decodeToken(token);
          var userls: UserLocalStorage = {
            isAuth: true,
            token: token,
            userName: decodedToken.given_name,
            userId: decodedToken.jti
          }
          localStorage.setItem('currentUser', JSON.stringify(userls));
          this.setUser(userls);
        }

        return token;
      });
  }

  logout(){
    localStorage.removeItem('currentUser');
    this.currentUser = new User();
    this.currentUserChange.next(this.currentUser);
    this.router.navigate(['']);
  }

  private setUser(userls: UserLocalStorage){
    this.currentUser = userls;
    this.currentUserChange.next(this.currentUser);
  }
}
