import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthorizationService } from '../authorization.service';
import { Userlogin } from '../../../models/userlogin';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  message: string;

  constructor(private autenticationService: AuthorizationService, private formBuilder: FormBuilder, private router: Router) { 
    this.loginForm = formBuilder.group({
      'UserName': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(50)])],
      'Password': [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(50)])],
    });
  }

  ngOnInit() {
  }

  login(user: Userlogin) {
    this.message = null;
    this.autenticationService.login(user)
      .subscribe(data => {
        this.router.navigate(['']);
      },
      error => {
        this.message = error.error;
      })
  }

}
