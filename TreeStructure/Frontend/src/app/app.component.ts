import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from './partials/authorization/authorization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Tree Structure';

  constructor(private router: Router, private authorizationService: AuthorizationService) {
    
  }

  logOut(){
    this.authorizationService.logout();
  }
}
