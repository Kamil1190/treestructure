import { Routes, RouterModule } from '@angular/router';

import { TreeviewComponent } from './partials/tree/treeview/treeview.component';
import { LoginComponent } from './partials/authorization/login/login.component';

const appRoutes: Routes = [
    { path: '', component: TreeviewComponent },
    { path: 'login', component: LoginComponent },
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);