export class Treenode {
    id: string;
    name: string;
    children: Array<Treenode>;
}
