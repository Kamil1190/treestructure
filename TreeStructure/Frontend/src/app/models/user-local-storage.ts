import { User } from './user';

export class UserLocalStorage extends User{
    token: string;
    userId: string;
}
