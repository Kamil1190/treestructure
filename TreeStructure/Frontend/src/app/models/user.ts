export interface IUser {
    isAuth: boolean;
    userName: string;
}

export class User implements IUser {
    isAuth: boolean = false;
    userName: string;
}
