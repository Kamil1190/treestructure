import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from './material/material.module';
import { MyTreeModule } from './partials/tree/mytree.module';
import { AuthorizationModule } from './partials/authorization/authorization.module';
import { AuthorizationService } from './partials/authorization/authorization.service';
import { JwtInterceptor } from './helpers/jwt-interceptor';
import { routing } from './app.routing';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    MyTreeModule,
    AuthorizationModule,
    routing
  ],
  providers: [
    AuthorizationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
