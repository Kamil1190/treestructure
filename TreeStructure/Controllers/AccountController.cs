﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using BusinessLogic.DTO;
using BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Web.Filters;

namespace Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private readonly IAccountService userService;

        public AccountController(IAccountService userService)
        {
            this.userService = userService;
        }

        [HttpPost]
        [Route("Login/")]
        [ValidateModel]
        public async Task<IActionResult> Login([FromBody]LoginDto model)
        {
            try
            {
                var token = await userService.Login(model);
                return Ok(token);
            } catch(ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}