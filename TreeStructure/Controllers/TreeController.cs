﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using BusinessLogic.DTO;
using BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Filters;

namespace Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Tree")]
    public class TreeController : Controller
    {
        private readonly ITreeService service;

        public TreeController(ITreeService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetRoot/")]
        public async Task<IActionResult> GetRoot()
        {
            var result = await service.GetRoot();
            return Ok(result);
        }

        [Authorize]
        [HttpPost]
        [Route("Create/{id:guid}")]
        [ValidateModel]
        public async Task<IActionResult> Create(Guid id, [FromBody]TreeNodeRequestDto nodeDto)
        {
            try
            {
                if(await service.Add(nodeDto, id))
                {
                    return Ok(true);
                }

                return BadRequest();
            }
            catch(ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpPut]
        [Route("Update/")]
        [ValidateModel]
        public async Task<IActionResult> Update([FromBody]TreeNodeResponseDto nodeDto)
        {
            try
            {
                if(await service.UpdateName(nodeDto))
                {
                    return Ok(true);
                }

                return BadRequest();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Authorize]
        [HttpDelete]
        [Route("Delete/{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                await service.Delete(id);
                return Ok(true);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}