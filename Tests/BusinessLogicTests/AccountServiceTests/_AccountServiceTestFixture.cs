﻿using BusinessLogic.Services;
using AutoFixture;
using Microsoft.AspNetCore.Identity;
using Moq;
using Tests.Helpers;
using BusinessLogic.Factories.Interfaces;

namespace Tests.BusinessLogic.AccountServiceTests
{
    public class _AccountServiceTestFixture : ServiceTestsFixtureBase
    {
        protected readonly Mock<UserManager<IdentityUser>> mockedUserManager;
        protected readonly Mock<SignInManager<IdentityUser>> mockedSignInManager;
        protected readonly Mock<ITokenFactory> mockedTokenFactory;

        public _AccountServiceTestFixture()
        {
            mockedUserManager = MockHelpers.MockUserManager<IdentityUser>();
            AutoFixture.Inject(mockedUserManager.Object);

            mockedSignInManager = MockHelpers.MockSignInManager<IdentityUser>(mockedUserManager);
            AutoFixture.Inject(mockedSignInManager.Object);

            mockedTokenFactory = AutoFixture.Freeze<Mock<ITokenFactory>>();
        }

        public Mock<UserManager<IdentityUser>> UserManagerMock => mockedUserManager;

        public Mock<SignInManager<IdentityUser>> SignInManagerMock => mockedSignInManager;

        public Mock<ITokenFactory> TokenFactoryMock => mockedTokenFactory;

        public AccountService Sut => AutoFixture.Create<AccountService>();
    }
}
