﻿using AutoFixture.Xunit2;
using BusinessLogic.DTO;
using FluentAssertions;
using Microsoft.AspNetCore.Identity;
using Moq;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Xunit;

namespace Tests.BusinessLogic.AccountServiceTests
{
    public class LoginMethodTests : IClassFixture<_AccountServiceTestFixture>
    {
        private readonly _AccountServiceTestFixture fixture;

        public LoginMethodTests(_AccountServiceTestFixture fixture)
        {
            this.fixture = fixture;
        }

        [Theory, AutoData]
        public async Task ValidLoginModel_ShouldGenerateToken(LoginDto login, IdentityUser user)
        {
            // Arrange
            fixture.SignInManagerMock
                .Setup(m => m.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), false, false))
                .ReturnsAsync(SignInResult.Success);

            fixture.UserManagerMock
                .Setup(m => m.FindByNameAsync(login.UserName))
                .ReturnsAsync(user);

            var token = "token_example";

            fixture.TokenFactoryMock
                .Setup(m => m.GenerateToken(It.Is<IdentityUser>(u => u.UserName.Equals(user.UserName))))
                .ReturnsAsync(token);

            // Act
            var result = await fixture.Sut.Login(login);

            // Assert
            result.ShouldBeEquivalentTo(token);
        }

        [Theory, AutoData]
        public async Task InValidLoginModel_ShouldThrowException(LoginDto login, IdentityUser user)
        {
            // Arrange

            fixture.UserManagerMock
                .Setup(m => m.FindByNameAsync(login.UserName))
                .Returns(Task.FromResult<IdentityUser>(null));

            // Act
            Func<Task> act = async () => await fixture.Sut.Login(login);

            // Assert
            act.ShouldThrow<ValidationException>();
        }
    }
}
