﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using BusinessLogic;
using Microsoft.EntityFrameworkCore;
using Moq;
using System.Linq;

namespace Tests.BusinessLogic
{
    public class ServiceTestsFixtureBase
    {
        protected readonly Mock<IMapper> MockedMapper;
        protected readonly IFixture AutoFixture;
        protected readonly MySqlContext context;

        public ServiceTestsFixtureBase()
        {
            AutoFixture = new Fixture().Customize(new AutoMoqCustomization());

            AutoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
                .ForEach(b => AutoFixture.Behaviors.Remove(b));

            AutoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            MockedMapper = AutoFixture.Freeze<Mock<IMapper>>();

            var options = new DbContextOptionsBuilder<MySqlContext>()
                .UseInMemoryDatabase(databaseName: "Test1")
                .Options;

            context = new MySqlContext(options);

            AutoFixture.Inject(context);
        }

        public Mock<IMapper> MapperMock => MockedMapper;

        public IFixture Auto => AutoFixture;

        public MySqlContext Context => context;
    }
}
