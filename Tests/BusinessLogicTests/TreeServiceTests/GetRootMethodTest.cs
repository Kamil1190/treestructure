﻿
using BusinessLogic;
using BusinessLogic.DTO;
using BusinessLogic.Models;
using FluentAssertions;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace Tests.BusinessLogic.TreeServiceTests
{
    public class GetRootMethodTest : IClassFixture<_TreeServiceTestFixture>
    {
        private readonly _TreeServiceTestFixture fixture;
        private readonly MySqlContext context;

        public GetRootMethodTest(_TreeServiceTestFixture fixture)
        {
            this.fixture = fixture;
            context = fixture.Context;
        }

        [Theory, TreeAutoData]
        public async Task TreeInDb_ShouldReturnTree(TreeNode tree, TreeNodeResponseDto treeDto)
        {
            // Arrange
            context.Tree.Add(tree);
            context.SaveChanges();

            fixture.MapperMock
                .Setup(m => m.Map<TreeNodeResponseDto>(It.IsAny<TreeNode>()))
                .Returns(treeDto);

            // Act
            var result = await fixture.Sut.GetRoot();

            // Assert
            result.Should().NotBeNull();
        }
    }
}
