﻿using BusinessLogic.Services;
using AutoFixture;

namespace Tests.BusinessLogic.TreeServiceTests
{
    public class _TreeServiceTestFixture : ServiceTestsFixtureBase
    {
        public TreeService Sut => AutoFixture.Create<TreeService>();
    }
}
