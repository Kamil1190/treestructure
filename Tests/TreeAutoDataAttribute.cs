﻿using AutoFixture;
using AutoFixture.Xunit2;
using BusinessLogic.DTO;
using BusinessLogic.Models;

namespace Tests
{
    public class TreeAutoDataAttribute : AutoDataAttribute
    {
        public TreeAutoDataAttribute() : 
            base(() => new Fixture().Customize(new TreeCustomization()))
        {
        }
    }

    public class TreeCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<TreeNode>(t => t
                .With(x => x.Name, fixture.Create<string>().ToLowerInvariant())
                .Without(x => x.Parent)
                .Without(x => x.Children));

            fixture.Customize<TreeNodeResponseDto>(t => t
                .With(x => x.Name, fixture.Create<string>().ToLowerInvariant())
                .Without(x => x.Children));
        }
    }
}
