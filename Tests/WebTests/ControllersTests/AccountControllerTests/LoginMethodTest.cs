﻿using AutoFixture.Xunit2;
using BusinessLogic.DTO;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Xunit;

namespace Tests.WebTests.ControllersTests.AccountControllerTests
{
    public class LoginMethodTest : IClassFixture<_AccountControllerTestsFixture>
    {
        private readonly _AccountControllerTestsFixture fixture;

        public LoginMethodTest(_AccountControllerTestsFixture fixture)
        {
            this.fixture = fixture;
        }

        [Theory, AutoData]
        public async Task ValidModel_ShouldReturnToken(LoginDto model)
        {
            // Arrange
            var token = "token_example";

            fixture.AccountServiceMock
                .Setup(x => x.Login(model))
                .ReturnsAsync(token);

            // Act
            var result = await fixture.Sut.Login(model);

            // Assert
            result.As<OkObjectResult>().Value.Should().Be(token);
        }

        [Theory, AutoData]
        public async Task InValidModel_ShouldReturnBadRequest(LoginDto model, ValidationException exception)
        {
            // Arrange
            fixture.AccountServiceMock
                .Setup(x => x.Login(model))
                .Throws(exception);

            // Act
            var result = await fixture.Sut.Login(model);

            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }
    }
}
