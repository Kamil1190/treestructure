﻿using AutoFixture;
using AutoFixture.AutoMoq;
using BusinessLogic.Services.Interfaces;
using Moq;
using Web.Controllers;

namespace Tests.WebTests.ControllersTests.AccountControllerTests
{
    public class _AccountControllerTestsFixture
    {
        private readonly AccountController _sut;
        private readonly Mock<IAccountService> _accountServiceMock;
        private readonly IFixture _autoFixture;

        public _AccountControllerTestsFixture()
        {
            _autoFixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            _accountServiceMock = _autoFixture.Freeze<Mock<IAccountService>>();

            _sut = new AccountController(_accountServiceMock.Object);
        }

        public AccountController Sut => _sut;

        public Mock<IAccountService> AccountServiceMock => _accountServiceMock;

        public IFixture Auto => _autoFixture;
    }
}
