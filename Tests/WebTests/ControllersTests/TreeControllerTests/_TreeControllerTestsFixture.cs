﻿using AutoFixture;
using AutoFixture.AutoMoq;
using BusinessLogic.Services.Interfaces;
using Moq;
using Web.Controllers;

namespace Tests.Web.ControllersTests.TreeControllerTests
{
    public class _TreeControllerTestsFixture
    {
        private readonly TreeController sut;
        private readonly Mock<ITreeService> treeServiceMock;
        private readonly IFixture autoFixture;

        public _TreeControllerTestsFixture()
        {
            autoFixture = new Fixture()
                .Customize(new AutoMoqCustomization());

            treeServiceMock = autoFixture.Freeze<Mock<ITreeService>>();

            sut = new TreeController(treeServiceMock.Object);
        }

        public TreeController Sut => sut;

        public Mock<ITreeService> TreeServiceMock => treeServiceMock;

        public IFixture Auto => autoFixture;
    }
}
