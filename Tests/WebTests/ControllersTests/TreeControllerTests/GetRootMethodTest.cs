﻿using BusinessLogic.DTO;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Tests.Web.ControllersTests.TreeControllerTests
{
    public class GetRootMethodTest : IClassFixture<_TreeControllerTestsFixture>
    {
        private readonly _TreeControllerTestsFixture fixture;

        public GetRootMethodTest(_TreeControllerTestsFixture fixture)
        {
            this.fixture = fixture;
        }

        [Theory, TreeAutoData]
        public async Task TreeInDb_ShouldReturnOk(TreeNodeResponseDto tree)
        {
            // Arrange
            fixture.TreeServiceMock
                .Setup(x => x.GetRoot())
                .ReturnsAsync(tree);

            // Act
            var result = await fixture.Sut.GetRoot();

            // Assert
            result.Should().BeOfType<OkObjectResult>();
        }
    }
}
